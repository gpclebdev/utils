<?php

namespace gpc\utils\yii2;


class AdAuthorize
{
    public static function login($adUsername, $adPassword, $autorizationUrl)
    {
        $userTable = 'user';
        if (isset(\Yii::$app->params['userTable']) && ($x = \Yii::$app->params['userTable'])) {
            $userTable = $x;
        }
        if (!$autorizationUrl) return false;
        $db = \Yii::$app->db;
        if (
            $db->createCommand("SHOW COLUMNS FROM `$userTable` LIKE 'ad_username'")->queryOne()
            && $db->createCommand("SHOW COLUMNS FROM `$userTable` LIKE 'active'")->queryOne()
            && ($res = $db->createCommand("SELECT * FROM $userTable WHERE ad_username=:u AND active=1", [':u' => $adUsername])->queryOne())
        ) {
            if (isset($res['id'])) {
//                var_dump($res);
//                exit;
                $id = $res['id'];
                $vars = [
                    'username' => $adUsername,
                    'password' => $adPassword
                ];
                try {
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $autorizationUrl);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($vars));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $server_output = curl_exec($ch);
                    curl_close($ch);
                    if ($server_output == '1') return true;
                } catch (\Exception $e) {

                }
            }
        }
        return false;
    }
}