<?php

namespace gpc\utils\logging;

use common\models\ActionLog;
use Yii;
use gpc\utils\security\UserLoginUtil;

/**
 * This is the model class for table "proforma_log".
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $description
 * @property string $modification_date
 * @property string $creation_date
 */
class ActionLogUtil extends ActionLog
{

    public static function logSingle($description)
    {
        $username = UserLoginUtil::getLoggedInUsername();
        $log = new ActionLog();
        $log->username = $username;
        $log->description = $description;
        $log->save();
    }
}
