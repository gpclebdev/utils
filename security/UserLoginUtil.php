<?php

namespace gpc\utils\security;


use ctblue\web\Utils\StringUtils;
use gpc\utils\security\models\LinksBaseModel;
use gpc\utils\security\models\UserBaseModel;
use gpc\utils\security\models\UserPermissionBaseModel;
use gpc\utils\security\models\UserTokenModel;
use gpc\utils\yii2\AdAuthorize;
use yii\helpers\Url;

class UserLoginUtil extends UserBaseModel
{
    /**
     * @var UserLoginUtil | bool
     */
    public static $loggedInUser = false;

    public static function adAuthorizationUrl()
    {
        return (isset(\Yii::$app->params['ad-authorization-url']) ? \Yii::$app->params['ad-authorization-url'] : false);
    }

    public function generateToken($appKey)
    {
        if ($application = LinksBaseModel::findOne(['static_title' => $appKey])) {
            if ($userToken = UserTokenModel::findOne(['user_id' => $this->id, 'app_id' => $application->id])) {
                return $userToken->token;
            } else {
                $userToken = new UserTokenModel();
                $userToken->user_id = $this->id;
                $userToken->app_id = $application->id;
                $userToken->token = StringUtils::generateRandomString(100);
                if ($userToken->save()) {
                    return $userToken->token;
                }
            }
        }
        return false;
    }

    /**
     * Login a user with a token (will be used in apis)
     * @param $token
     * @param $appKey
     * @return bool
     */
    public static function loginWithToken($token, $appKey)
    {
        if ($token && $appKey) {
            if ($application = LinksBaseModel::findOne(['static_title' => $appKey])) {
                if ($userToken = UserTokenModel::findOne(['token' => $token, 'app_id' => $application->id])) {
                    if (UserLoginUtil::$loggedInUser = UserLoginUtil::findOne([
                        'id' => $userToken->user_id,
                        'active' => 1,
                        'status' => 10
                    ])) {
                        return true;
                    }
                }
            }
        }
    }

    /**
     * Checks if a user has access
     * @param $access
     * @param bool $userId
     * @param bool $appKey
     * @return bool|mixed
     */
    public static function checkAccess($access, $userId = false, $appKey = false)
    {
        return UserPermissionBaseModel::check($access, $userId, $appKey);
    }

    private static $cacheByUsername = [];

    public static function findOneByUsername($username)
    {
        if (isset(UserLoginUtil::$cacheByUsername[$username]) && ($user = UserLoginUtil::$cacheByUsername[$username])) return $user;
        if ($user = UserLoginUtil::findOne(['username' => $username])) {
            UserLoginUtil::$cacheByUsername[$username] = $user;
            return $user;
        }
        return false;
    }

    public static function login($username, $password, $appKey = false)
    {
        if ($username && $password) {
            if (AdAuthorize::login($username, $password, UserLoginUtil::adAuthorizationUrl())) {
                //find the user by ad username
                if (UserLoginUtil::$loggedInUser = UserLoginUtil::findOne(['ad_username' => $username, 'active' => 1, 'status' => 10])) {
                    //generate the token
                    if ($appKey = \Yii::$app->params['app-id']) {
                        UserLoginUtil::$loggedInUser->generateToken($appKey);
                    }
                    \Yii::$app->session->set('user', UserLoginUtil::$loggedInUser);
                    return true;
                }
            }
        }
        return false;
    }

    public static function getLoggedInUsername()
    {
        if (UserLoginUtil::isLoggedIn()) {
            /** @var UserBaseModel $user */
            if ($user = UserLoginUtil::$loggedInUser) {
                return $user->username;
            }
        }
        return false;
    }

    /**
     * @return bool|UserBaseModel
     */
    public static function loggedInUser()
    {
        if (UserLoginUtil::isLoggedIn()) {
            /** @var UserBaseModel $user */
            if ($user = UserLoginUtil::$loggedInUser) {
                return $user;
            }
        }
        return false;
    }

    /**
     * @return bool|int
     */
    public static function loggedInUserId()
    {
        if (UserLoginUtil::isLoggedIn()) {
            /** @var UserBaseModel $user */
            if ($user = UserLoginUtil::$loggedInUser) {
                return $user->id;
            }
        }
        return false;
    }


    public static function isLoggedIn()
    {
        if ($user = \Yii::$app->session->get('user')) {
            UserLoginUtil::$loggedInUser = $user;
            return true;
        }
        return false;
    }

    public static function logoutAndRedirectToLogin($urlParams = '')
    {
        UserLoginUtil::logout();
        UserLoginUtil::redirectToLogin($urlParams);
        exit;
    }

    public static function logout()
    {
        $session = \Yii::$app->session;
        $session->remove('user');
        UserLoginUtil::$loggedInUser = false;
    }

    public static function redirectToLogin($urlParams = '')
    {
        header('Location:' . Url::toRoute(['site/login']) . $urlParams, true, 302);
    }

    public static function isGuest()
    {
        $session = \Yii::$app->session;
        if (!$session->get('user')) {
            return true;
        }
        return false;
    }
}