<?php


namespace gpc\utils\security\models;


use gpc\utils\security\models\LinksBaseModel;
use gpc\utils\security\models\UserBaseModel;
use Yii;

/**
 * This is the model class for table "user_token".
 *
 * @property int $id
 * @property int $user_id
 * @property int $app_id
 * @property string|null $token
 * @property string $modification_date
 * @property string $creation_date
 *
 * @property User $user
 * @property Links $app
 */
class UserTokenModel extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->dbUserManagement;
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_token';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'app_id'], 'required'],
            [['user_id', 'app_id'], 'integer'],
            [['modification_date', 'creation_date'], 'safe'],
            [['token'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserBaseModel::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['app_id'], 'exist', 'skipOnError' => true, 'targetClass' => LinksBaseModel::className(), 'targetAttribute' => ['app_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'app_id' => 'App ID',
            'token' => 'Token',
            'modification_date' => 'Modification Date',
            'creation_date' => 'Creation Date',
        ];
    }

    /**
     * Gets query for [[UserBaseModel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserBaseModel::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[App]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApp()
    {
        return $this->hasOne(LinksBaseModel::className(), ['id' => 'app_id']);
    }
}
