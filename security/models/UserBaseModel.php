<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 16-Jan-20
 * Time: 4:47 PM
 */

namespace gpc\utils\security\models;


use gpc\security\UserPermission;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string|null $ad_username
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property string $email
 * @property int $status
 * @property int|null $active
 * @property int $created_at
 * @property int $updated_at
 * @property string|null $verification_token
 *
 * @property UserPermission[] $userPermissions
 */
class UserBaseModel extends \yii\db\ActiveRecord
{
    public static function getDb()
    {
        return \Yii::$app->dbUserManagement;
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['status', 'active', 'created_at', 'updated_at'], 'integer'],
            [['username', 'ad_username', 'password_hash', 'password_reset_token', 'email', 'verification_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'ad_username' => 'Ad Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'active' => 'Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'verification_token' => 'Verification Token',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPermissions()
    {
        return $this->hasMany(UserPermission::className(), ['user_id' => 'id']);
    }
}