<?php

namespace gpc\utils\mail;


class SendMail
{
    public static function sendMailAsTable($emails, $subject, $table, $includeCount)
    {
        $body = "";
        if ($includeCount) {
            $body .= "There are " . count($table) . " errors<br><br>";
        }
        $body .= "<table style='border-radius: 10px;border: 1px solid black;width: 100%;color: black;'>";
        foreach ($table as $row) {
            $body .= "<tr style='border: 1px solid black;'>";
            foreach ($row as $col) {
                $body .= "<td style='border: 1px solid black;'>" . $col . "</td>";
            }
            $body .= "</tr>";
        }
        $body.='</table>';

        SendMail::sendMail($emails, $subject, $body);
    }

    /**
     *  Sends an email using the http php server
     * @param $emails
     * @param $subject
     * @param $body
     * @param $sender string ".po" to use the purchase order noreply sender
     * @param $important
     * @return mixed
     */
    public static function sendMail($emails, $subject, $body, $sender=false, $important=false)
    {
        if (defined('YII_ENV')) {
            if ((YII_ENV == 'dev') && ($devEmail = \Yii::$app->params['dev-email'])) {
                if (is_array($emails)) $emails = implode(',', $emails);
                $body = '<b>This email should have been sent to:<br>' . $emails . '</b><br><br>' . $body;
                $emails = $devEmail;
            }
        }
        if (isset(\Yii::$app->params['email-dev-data'])) {
            ob_start();
            ?>
            <br><br><br>
            ------------------
            <u>Dev information:</u><br>
            This email has been sent to <?php echo $emails ?>
            <?php
            $body .= ob_get_clean();
        }
        if (is_array($emails)) $emails = implode(',', $emails);
        $url = 'http://192.168.0.9:81/phpmailer/local.php?';
        $myvars = 'email=' . urlencode($emails) . '&subject=' . urlencode($subject) . '&body=' . urlencode($body);
        if ($sender) {
            $myvars .= '&sender=' . $sender;
        }
        if ($important) {
            $myvars .= '&important=1';
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $myvars);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        return $response;
    }
}